package com.example.polina.framebyframe;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imgView1;
    private AnimationDrawable frameAnimation1;

    private ImageView imgView2;
    private AnimationDrawable frameAnimation2;
    private final int duration = 250;

    private SeekBar seekBar;

    private RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "Let's begin!", Toast.LENGTH_LONG ).show();

        //heart animation

        //set xml with frames as background image for ImageView
        imgView1 = (ImageView)findViewById(R.id.image_anim);
        imgView1.setImageBitmap(null);
        imgView1.setBackgroundResource(R.drawable.draw);

        //create and start frame animation
        frameAnimation1 = (AnimationDrawable) imgView1.getBackground();
        frameAnimation1.start();

        //snail animation

        imgView2 = (ImageView)findViewById(R.id.snail_anim);
        startSnailFrameAnimation();

        //rating bar:
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);

        //seek bar to change snail animation alpha
        seekBar = (SeekBar)findViewById(R.id.set_alpha);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                frameAnimation2.setAlpha(progress);
                ratingBar.setNumStars(5);
                ratingBar.setRating((float)progress/50);
                Toast.makeText(MainActivity.this, "Progress: " + progress, Toast.LENGTH_SHORT ).show();
                Toast.makeText(MainActivity.this, "Rating: " + ratingBar.getRating(), Toast.LENGTH_SHORT ).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this, "Started tracking seekbar", Toast.LENGTH_SHORT ).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this, "Stopped tracking seekbar", Toast.LENGTH_SHORT ).show();
            }
        });
    }

    private void startSnailFrameAnimation() {
        BitmapDrawable frame1 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_1);
        BitmapDrawable frame2 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_2);
        BitmapDrawable frame3 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_3);
        BitmapDrawable frame4 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_4);
        BitmapDrawable frame5 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_5);
        BitmapDrawable frame6 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_6);
        BitmapDrawable frame7 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_7);
        BitmapDrawable frame8 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_8);
        BitmapDrawable frame9 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_9);
        BitmapDrawable frame10 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_10);
        BitmapDrawable frame11 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_11);
        BitmapDrawable frame12 = (BitmapDrawable)getResources().getDrawable(R.drawable.snail_12);

        frameAnimation2 = new AnimationDrawable();
        frameAnimation2.setOneShot(false);
        frameAnimation2.addFrame(frame1, duration);
        frameAnimation2.addFrame(frame2, duration);
        frameAnimation2.addFrame(frame3, duration);
        frameAnimation2.addFrame(frame4, duration);
        frameAnimation2.addFrame(frame5, duration);
        frameAnimation2.addFrame(frame6, duration);
        frameAnimation2.addFrame(frame7, duration);
        frameAnimation2.addFrame(frame8, duration);
        frameAnimation2.addFrame(frame9, duration);
        frameAnimation2.addFrame(frame10, duration);
        frameAnimation2.addFrame(frame11, duration);
        frameAnimation2.addFrame(frame12, duration);

        imgView2.setBackgroundDrawable(frameAnimation2);

        frameAnimation2.start();
    }
}
