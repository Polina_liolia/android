package com.example.polina.simplesort;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputParser {
    private String input;
    private String regexp = "n=(\\d){1}.?(\\((?:-?\\d,*)+\\))";
    private Pattern pattern;

    public InputParser(String input) {
        this.input = input;
        pattern = Pattern.compile(this.regexp);
    }

    //validate input string first
    public boolean IsInputValid(){
        Matcher matcher = pattern.matcher(this.input);
        return matcher.matches();
    }

    //getting numbers from validated string
    public double[] getNumbers(){
        Matcher matcher = pattern.matcher(this.input);
        if(matcher.matches()){
            String total = matcher.group(1);
            int n = Integer.valueOf(total); //number of elements
            String[] numbersStr = matcher.group(2).replaceAll("[()]", "").split(","); //elements
            double[] numbers = new double[numbersStr.length];
            //converting numbers strings to double and pushing to array
            for (int i = 0; i < numbers.length; i++){
                numbers[i] = Double.parseDouble(numbersStr[i]);
            }
            return numbers;
        }
        else throw new IllegalArgumentException("No match fount in input string");
    }

}
