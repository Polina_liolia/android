package com.example.polina.simplesort;

public class SortAgent {
    static double[] Sort(double[] initialArray, boolean isIncremental){
        double temp;
        for (int i = 1; i < initialArray.length; i++) {
            for (int j = i; j > 0; j--) {
                if (initialArray[j] < initialArray [j - 1]) {
                    temp = initialArray[j];
                    initialArray[j] = initialArray[j - 1];
                    initialArray[j - 1] = temp;
                }
            }
        }
        return initialArray;
    }
}
