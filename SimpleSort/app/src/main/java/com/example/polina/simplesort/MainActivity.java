package com.example.polina.simplesort;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    TextView textView;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.txtResult);
        btn = (Button)findViewById(R.id.btnResult);
        editText = (EditText) findViewById(R.id.txtInput);

        //creating event listener
        View.OnClickListener btnClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View arg){
                String text = editText.getText().toString();
                InputParser parser = new InputParser(text);
                if(parser.IsInputValid()){
                    double[] numbersSorted = SortAgent.Sort(parser.getNumbers(), true);
                    StringBuilder sortResultStr = new StringBuilder();
                    sortResultStr.append("n=");
                    sortResultStr.append(numbersSorted.length);
                    sortResultStr.append("(");
                    for(int i = 0; i < numbersSorted.length; i++){
                        sortResultStr.append(numbersSorted[i]);
                        if (i != numbersSorted.length - 1){
                            sortResultStr.append(",");
                        }
                        else{
                            sortResultStr.append(")");
                        }
                    }
                    textView.setText(sortResultStr.toString());
                }
                else{
                    textView.setText("Wrong input!");
                }
            }
        };

        //adding event listener to button
        btn.setOnClickListener(btnClickListener);
    }



}
