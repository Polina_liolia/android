package com.example.polina.layoutfromjava;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class MainActivity extends Activity {

    LinearLayout linearLayoutMainLay;
    LinearLayout linearLayout1;
    LinearLayout linearLayout2;
    LayoutParams layoutParamsMainLay = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    LayoutParams layoutParamsLay = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
    LayoutParams layoutParamsBtn = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    Button[] button1;
    Button[] button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linearLayoutMainLay = new LinearLayout(this);

        linearLayout1 = new LinearLayout(this);
        linearLayout1.setBackgroundResource(R.mipmap.ic_launcher);
        linearLayout1.setOrientation(LinearLayout.HORIZONTAL);

        linearLayout2 = new LinearLayout(this);

        linearLayoutMainLay.addView(linearLayout1, layoutParamsLay);
        linearLayoutMainLay.addView(linearLayout2,layoutParamsLay);

        //buttons
        button1 = new  Button[5];
        for(int i = 0; i < 5; i++)
        {
            button1[i] = new Button(this);
            button1[i].setId(i);
            button1[i].setLayoutParams(layoutParamsBtn);
            linearLayout1.addView(button1[i]);
        }
        button2 = new  Button[5];
        for(int i = 0; i < 5; i++)
        {
            button2[i] = new Button(this);
            button2[i].setId(i);
            button2[i].setLayoutParams(layoutParamsBtn);
            button2[i].setBackgroundResource(R.mipmap.ic_launcher);
            linearLayout2.addView(button2[i]);
        }

        setContentView(linearLayoutMainLay, layoutParamsMainLay);
    }


}
