package com.example.polina.adapters;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "My Logs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //simple strings list
        configureArrayAdapter();
        //complex data
        configureSimpleAdapter();
        //grouped lists
        configureSimpleExpandableListAdapter();
        //adapter for sql query or inner storage data output
        configureSimpleCursorAdapter();

    }

    private void configureSimpleAdapter() {
        //complex data
        ListView listViewComplex = findViewById(R.id.listViewComplex);
        ArrayList<HashMap<String, Object>> myPad = new ArrayList<>(); //lists array

        String myNote1 = "Note1";
        String myNote2 = "Note2";
        String myImg = "myImg";

        //first data group
        HashMap<String, Object> hm = new HashMap<>(); //objects list
        hm.put(myNote1, "Cakes"); //name
        hm.put(myNote2, "add_1"); //description
        hm.put(myImg, R.drawable.ic_launcher_foreground); //img
        myPad.add(hm);

        //second data group
        hm = new HashMap<>(); //objects list
        hm.put(myNote1, "Drinks"); //name
        hm.put(myNote2, "add_2"); //description
        hm.put(myImg, R.drawable.ic_launcher_background); //img
        myPad.add(hm);

        //third data group
        hm = new HashMap<>(); //objects list
        hm.put(myNote1, "Meat"); //name
        hm.put(myNote2, "add_3"); //description
        hm.put(myImg, R.drawable.ic_launcher_background); //img
        myPad.add(hm);

        SimpleAdapter adapterComplex = new SimpleAdapter(this, myPad, R.layout.my_list_complex,
                new String[]{ //names array
                    myNote1,
                    myNote2,
                    myImg
                },
                new int[]{  //forms array
                    R.id.textViewComplex,
                    R.id.textViewComplex2,
                    R.id.img
                });
        //display all objects:
        listViewComplex.setAdapter(adapterComplex);
        listViewComplex.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d(TAG, "item click: position " + position + ", id " + id);
            }
        });
    }

    private void configureArrayAdapter() {
        //simple string data
        ListView listViewSimple = findViewById(R.id.listView);
        final String[] recipes = new String[]{
                "Cakes", "Chickens", "Meat", "Souses", "Eggs", "Potato", "Cookies"
        };
        //set adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.my_list, recipes);
        listViewSimple.setAdapter(adapter);

        //set on click event handler
        listViewSimple.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d(TAG, "item click: position " + position + ", id " + id);
                Toast.makeText(getApplicationContext(), ((TextView)view).getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void configureSimpleExpandableListAdapter() {
        String NAME = "NAME";
        String IS_EVEN = "IS_EVEN";
        //grouped list
        ArrayList<Map<String,String>> groupData = new ArrayList<>();
        ArrayList<List<Map<String,String>>> childData = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            Map<String, String> currentGroupMap = new HashMap<>();
            groupData.add(currentGroupMap);
            currentGroupMap.put(NAME, "Item " + i);
            currentGroupMap.put(IS_EVEN, (i%2 == 0) ? "Even" : "Odd");

            List<Map<String, String>> children = new ArrayList<>();
            for(int j = 0; j < 3; j++){
                Map<String, String> currentChildMap = new HashMap<>();
                children.add(currentChildMap);
                currentGroupMap.put(NAME, "Subitem " + j);
                currentGroupMap.put(IS_EVEN, (j%2 == 0) ? "Even" : "Odd");
            }
            childData.add(children);
        }
        //set up adapter
        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this,
                groupData,
                R.layout.my_expandable_list,
                new String[]{NAME, IS_EVEN},
                new int[] {R.id.textViewEx, R.id.textViewEx2},
                childData,
                R.layout.my_expandable_list,
                new String[]{NAME, IS_EVEN},
                new int[] {R.id.textViewEx, R.id.textViewEx2}
                );
        ExpandableListView listView = findViewById(R.id.listViewExpandable);
        listView.setAdapter(adapter);
    }

    private void configureSimpleCursorAdapter() {
        ListView listView = findViewById(R.id.listViewCursor);

        Cursor cursor = managedQuery(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null );

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.my_list_cursor,
                cursor,
                new String[]{
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                },
                new int[] {
                    R.id.textViewCursor,
                    R.id.textViewCursor2,
                    R.id.textViewCursor3
        });
        listView.setAdapter(adapter);

    }


}
