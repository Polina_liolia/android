package com.example.polina.xmlorjava;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleableRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;


public class GameActivityTable extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG="myLogs";
    TableLayout tableLayout;
    TableRow [] tableRow;
    Button[] btnNew;

    final int k = 56, //btns number
    m = 8, //rows number
    n = 7; //columns number

    int count = 0,
    countK = 0;


    private static final Integer[] icons = {R.drawable._2_of_clubs, R.drawable._2_of_diamonds};
    Resources res = getResources();
    TypedArray icon = res.obtainTypedArray(R.array.icon);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_game_table);
        Log.d(TAG, "GameActivityTable onCreate");

        tableLayout = new TableLayout(this);
        tableRow = new TableRow[m];
        btnNew = new Button[k];

        for(int i = 0; i < k; i++)
        {
            btnNew[i] = new Button(this);
            btnNew[i].setBackgroundResource(R.drawable.cardback);
            btnNew[i].setOnClickListener(this);
            btnNew[i].setId(i);
            btnNew[i].setLayoutParams(new TableRow.LayoutParams(20,40, 1.0f));
        }

        for (int i = 0; i < m; i++){
            tableRow[i] = new TableRow(this);     //create table row
            for (int j = 0; j < n; j++){                //add buttons to row
                tableRow[i].addView(btnNew[countK++]);
            }
        }

        for (int i = 0; i < m; i++){            //add row to layout
            tableLayout.addView(tableRow[i]);
        }
    }

    @Override
    public void onClick(View view) {
        @StyleableRes
        int resIndex = 0;

        for(int i = 0; i < k; i++){
            if(i == view.getId()){
                if(count%2 == 0){
                    btnNew[i].setBackgroundResource(icon.getResourceId(resIndex, 0));
                }
                else{
                    btnNew[i].setBackgroundResource(icon.getResourceId(resIndex+1, 0));
                }
                count++;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.file3: restart(); return true;
            case R.id.file4: quit(); return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void quit() {
        finish();
    }

    private void restart() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);

    }


}
