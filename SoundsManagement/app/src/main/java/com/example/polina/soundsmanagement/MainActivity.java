package com.example.polina.soundsmanagement;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private final String TAG = "myLogs";
    int soundIdJava, soundIdRes;
    SoundPool soundPool;
    AssetManager assetManager;
    AssetFileDescriptor assetFileDescriptor;
    Button btn1, btn2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        btn2 = findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        }
        else{
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            soundPool = new SoundPool.Builder().setAudioAttributes(attributes).build();
        }
        //set up sound with resources:
        soundIdRes = soundPool.load(this, R.raw.success_robot, 1);
        //set up sound with java:
        soundIdJava = soundPool.load(assetFileDescriptor, 1);
    }

    @Override
    public void onClick(View view) {
    switch (view.getId()){
        case R.id.btn1: soundPool.play(soundIdJava, 1, 1, 1, 0, 1); break;
        case R.id.btn2: soundPool.play(soundIdRes, 1.0f, 1.0f, 1, 0, 1.0f); break;
    }
    }
}
