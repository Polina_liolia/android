package com.example.polina.squarerootequalty;

public class SquareEqualty {

    int a;
    int b;
    int c;


    public SquareEqualty(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public SquareEqualtyRoots GetResult(){
        double D = Math.pow(this.b, 2) - 4 * this.a * this.c;
        SquareEqualtyRoots squareEqualtyRoots;
        if (D > 0){
            double x1 =(double) ((-this.b + Math.sqrt(D)) / (2 * this.a));
            double x2 = (double) ((-this.b - Math.sqrt(D)) / (2 * this.a));
            squareEqualtyRoots = new SquareEqualtyRoots(x1, x2, true);
        }
        else if (D == 0){
           double x1 =  (double) (-this.b / (2 * this.a));
            squareEqualtyRoots = new SquareEqualtyRoots(x1, x1, true);
        }
        else {  //no roots
            squareEqualtyRoots = new SquareEqualtyRoots(-1, -1, false);
        }
        return squareEqualtyRoots;
    }
}
