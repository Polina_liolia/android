package com.example.polina.squarerootequalty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText inputText;
    Button btn;
    TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //getting UI components refs:
        inputText = (EditText) findViewById(R.id.inputText);
        btn = (Button) findViewById(R.id.btn);
        txtResult = (TextView) findViewById(R.id.txtResult);

        //creating event listener
        View.OnClickListener btnClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View arg){
                String text = inputText.getText().toString();
                int[]args = InputParser.GetArgs(text, "=", ", ");
                SquareEqualty squareEqualty = new SquareEqualty(args[0], args[1], args[2]);
                SquareEqualtyRoots roots = squareEqualty.GetResult();
                String resultText;
                if(roots.hasRoot){
                    resultText = String.format("x1 = %f, x2 = %f", roots.getX1(), roots.getX2());
                }
                else {
                    resultText = "No roots";
                }
                txtResult.setText(resultText);
            }
        };

        //adding event listener to button
        btn.setOnClickListener(btnClickListener);

    }
}
