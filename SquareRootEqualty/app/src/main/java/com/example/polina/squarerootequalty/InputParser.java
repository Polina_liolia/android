package com.example.polina.squarerootequalty;

import android.animation.TypeConverter;

public class InputParser {

    //takes input - string to parse
    //divider1 - first divider, used for input (for a=1 divider1 is "=")
    //divider2 - elements divider, used for input (for a=1, b=2 divider1 is ", ")
    //returns array of 3 elements, where [0] - a, [1] - b, [2] - c
    static int[] GetArgs(String input, String divider1, String divider2){
        int[] args = new int[3];
        String[] elements = input.split(divider2);
        for(int i = 0; i < elements.length && i < 3; i++ ){
            String[]results = elements[i].split(divider1);
            if(results.length > 1){
                args[i] = Integer.valueOf(results[1]);
            }
        }
        return args;
    }
}
