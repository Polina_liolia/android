package com.example.polina.squarerootequalty;

public class SquareEqualtyRoots {
    double x1;
    double x2;
    boolean hasRoot;

    public SquareEqualtyRoots(double x1, double x2, boolean hasRoot) {
        this.x1 = x1;
        this.x2 = x2;
        this.hasRoot = hasRoot;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public boolean isHasRoot() {
        return hasRoot;
    }

    public void setHasRoot(boolean hasRoot) {
        this.hasRoot = hasRoot;
    }
}
