package com.example.polina.workwithfiles;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "myLogs";
    TextView fileInfoDeviceMem;
    EditText txtNameDeviceMem, txtInfoDeviceMem;
    Button btnCreateFileDeviceMem, btnSaveInfoDeviceMem, btnReadInfoDeviceMem, btnDeleteFileDeviceMem;

    TextView fileInfoSDMem;
    EditText txtNameSDMem, txtPathSDMem, txtInfoSDMem;
    Button btnSaveFileSDMemCommon, btnSaveFileSDMemApp, btnReadInfoSDMem, btnDeleteFileSDMem;
    private static final int REQUEST_WRITE_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v(TAG, "Draft");
        //____DEVICE MEMORY UI ELEMENTS____
        fileInfoDeviceMem = findViewById(R.id.txt_FileInfoEm);
        txtNameDeviceMem = findViewById(R.id.txt_EditNameEm);
        txtInfoDeviceMem = findViewById(R.id.txt_EditInfoEm);

        btnCreateFileDeviceMem = findViewById(R.id.btn_CreateFileEm);
        btnCreateFileDeviceMem.setOnClickListener(this);

        btnSaveInfoDeviceMem = findViewById(R.id.btn_SaveInfoEm);
        btnSaveInfoDeviceMem.setOnClickListener(this);

        btnReadInfoDeviceMem = findViewById(R.id.btn_ReadInfoEm);
        btnReadInfoDeviceMem.setOnClickListener(this);

        btnDeleteFileDeviceMem = findViewById(R.id.btn_DeleteFileEm);
        btnDeleteFileDeviceMem.setOnClickListener(this);

        //____SD MEMORY UI ELEMENTS____
        fileInfoSDMem = findViewById(R.id.txt_FileInfoSD);
        txtNameSDMem = findViewById(R.id.txt_EditNameSD);
        txtPathSDMem = findViewById(R.id.txt_EditPathSD);
        txtInfoSDMem = findViewById(R.id.txt_EditInfoSD);

        btnSaveFileSDMemCommon = findViewById(R.id.btn_SaveFileSDCommon);
        btnSaveFileSDMemCommon.setOnClickListener(this);

        btnSaveFileSDMemApp = findViewById(R.id.btn_SaveFileSDApp);
        btnSaveFileSDMemApp.setOnClickListener(this);

        btnReadInfoSDMem = findViewById(R.id.btn_ReadInfoSD);
        btnReadInfoSDMem.setOnClickListener(this);

        btnDeleteFileSDMem = findViewById(R.id.btn_DeleteFileSD);
        btnDeleteFileSDMem.setOnClickListener(this);

        //____________PERMISSIONS REQUEST___________
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M //more than API level 23
        && permission != PackageManager.PERMISSION_GRANTED ) //permission was not granted yet
        {
            Log.d(TAG, "Permission to record denied");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_WRITE_STORAGE:{
                if(grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Log.d(TAG, "Permission has been denied by user");
                }
                else{
                    Log.d(TAG, "Permission has been granted by user");
                }
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //____DEVICE MEMORY ACTIONS____
            case R.id.btn_CreateFileEm: {
                this.createFile(txtNameDeviceMem.getText().toString());
                break;
            }
            case R.id.btn_SaveInfoEm: {
                this.saveFile(txtNameDeviceMem.getText().toString(), txtInfoDeviceMem.getText().toString());
                break;
            }
            case R.id.btn_ReadInfoEm: {
                String info = this.readFile(txtNameDeviceMem.getText().toString());
                this.fileInfoDeviceMem.setText(info);
                break;
            }
            case R.id.btn_DeleteFileEm: {
                this.deleteMyFile(txtNameDeviceMem.getText().toString());
                break;
            }
            //____SD MEMORY ACTIONS____
            case R.id.btn_SaveFileSDCommon: {
                this.saveFileSDCommon(txtNameSDMem.getText().toString(),
                        txtPathSDMem.getText().toString(),
                        txtInfoSDMem.getText().toString());
                break;
            }
            case R.id.btn_SaveFileSDApp: {
                this.saveFileSDApp(txtNameSDMem.getText().toString(),
                        txtPathSDMem.getText().toString(),
                        txtInfoSDMem.getText().toString());
                break;
            }
            case R.id.btn_ReadInfoSD: {
                String info = this.readFileSD(txtNameSDMem.getText().toString(),
                        txtPathSDMem.getText().toString());
                this.fileInfoSDMem.setText(info);
                break;
            }
            case R.id.btn_DeleteFileSD: {
                this.deleteMyFileSD(txtNameSDMem.getText().toString(),
                        txtPathSDMem.getText().toString());
                break;
            }
        }
    }

    //____________EMBEDDED DEVICE MEMORY___________________
    void createFile(String fileName) {
        Log.d(TAG, "Create file: " + fileName);
        try {
            FileOutputStream outputStream = this.openFileOutput(fileName, this.MODE_PRIVATE); //create or recreate file (accessible for app only)
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void saveFile(String fileName, String information) {
        try {
            FileOutputStream outputStream = this.openFileOutput(fileName, this.MODE_APPEND); //add info
            outputStream.write(information.getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Save file: " + fileName);
    }

    String readFile(String fileName) {
        Log.d(TAG, "Read file: " + fileName);
        String text = "";
        try {
            InputStream inputStream = openFileInput(fileName);
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String receiveString = "";
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                    Log.d(TAG, "receive string  = " + receiveString);
                }
                inputStream.close();
                text = stringBuilder.toString();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    void deleteMyFile(String fileName) {
        Log.d(TAG, "Delete file: " + fileName);
        this.deleteFile(fileName);
    }

    //____________SD CARD MEMORY___________
    //saves file in common directory (it will not be removed with app)
    private void saveFileSDCommon(String fileName, String path, String info) {
        Log.d(TAG, "Save file in common folder: " + path + fileName);
        //check if SD is available:
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            Log.d(TAG, "SD cars unavailable");
            return;
        }
        File sdDirectory = Environment.getExternalStorageDirectory();
        sdDirectory = new File(sdDirectory.getAbsolutePath() + "/" + path); //add user's catalog to path
        sdDirectory.mkdirs(); // create catalog

        File sdFile = new File(sdDirectory, fileName);//create file in a directory
        try{
            BufferedWriter bufWriter = new BufferedWriter(new FileWriter(sdFile)); //open input stream
            bufWriter.write(info);
            bufWriter.close();
            Log.d(TAG, "File saved on SD: " + sdFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //saves file in app folder (it will be removed with app)
    private void saveFileSDApp(String fileName, String path, String info) {
        Log.d(TAG, "Save file in app folder: " + path + fileName);
        //check if SD is available:
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            Log.d(TAG, "SD cars unavailable");
            return;
        }
        File sdDirectory = new File(this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), path); //add user's catalog to path
        if(!sdDirectory.mkdirs()){ // create catalog
            Log.d(TAG, "Directory not created");
            return;
        }

        File sdFile = new File(sdDirectory, fileName);//create file in a directory
        try{
            BufferedWriter bufWriter = new BufferedWriter(new FileWriter(sdFile)); //open input stream
            bufWriter.write(info);
            bufWriter.close();
            Log.d(TAG, "File saved on SD: " + sdFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String readFileSD(String fileName, String path) {
        Log.d(TAG, "Read file : " + path + fileName);
        StringBuilder text = new StringBuilder();
        //check SD:
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            Log.d(TAG, "SD cars unavailable");
            return text.toString();
        }
        //try to find file in common directory:
        File sdDirectory = Environment.getExternalStorageDirectory();
        sdDirectory = new File(sdDirectory.getAbsolutePath() + "/" + path);
        File sdFile = new File(sdDirectory, fileName);//find file in a directory
        if(!sdDirectory.exists() || !sdFile.exists()) {
            Log.d(TAG, "Directory or file does not exist in common folder");
            //check app folder
            sdDirectory = new File(this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), path); //add user's catalog to path
            sdFile = new File(sdDirectory, fileName);//create file in a directory
            if (!sdDirectory.exists() || !sdFile.exists()) { // catalog does not exist
                Log.d(TAG, "Directory or file does not exist in app folder");
            }
        }
        if (sdDirectory.exists() && sdFile.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(sdFile));
                String line;
                while ((line = reader.readLine()) != null) {
                    text.append(line + '\n');
                }
                reader.close();
            } catch (IOException e) {
                Log.e(TAG, "Error occured while reading text file!!");
            }
        }
        return text.toString();
    }

    private void deleteMyFileSD (String fileName, String path){
        Log.d(TAG, "Delete file : " + path + fileName);
        //check SD:
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            Log.d(TAG, "SD cars unavailable");
            return;
        }
        //try to find file in common directory:
        File sdDirectory = Environment.getExternalStorageDirectory();
        sdDirectory = new File(sdDirectory.getAbsolutePath() + "/" + path);
        File sdFile = new File(sdDirectory, fileName);//find file in a directory
        if(!sdDirectory.exists() || !sdFile.exists()) {
            Log.d(TAG, "Directory or file does not exist in common folder");
            //check app folder
            sdDirectory = new File(this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), path); //add user's catalog to path
            sdFile = new File(sdDirectory, fileName);//create file in a directory
            if (!sdDirectory.exists() || !sdFile.exists()) { // catalog does not exist
                Log.d(TAG, "Directory or file does not exist in app folder");
            }
        }
        if (sdDirectory.exists() && sdFile.exists()) {
            sdFile.delete();
            sdDirectory.delete();
        }
    }
}
