package com.example.polina.mypad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RecipeActivity extends AppCompatActivity implements View.OnClickListener {

    EditText recipeText;
    Button acceptCangesBtn, backBtn;
    TextView recipeNameTxt;

    String recipeId;
    String recipeName;
    String categoryName;

    HelperMethodClass helperMethodClass = new HelperMethodClass();
    String dbName = "cooking.db";
    String sqlQuery;
    int currentRow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        Intent intent = getIntent();
        this.recipeId = intent.getStringExtra("recipe_id");
        this.recipeName = intent.getStringExtra("recipe_name");
        this.categoryName = intent.getStringExtra("category_name");
        this.currentRow = intent.getIntExtra("current_row", 0);

        sqlQuery = "create table if not exists " + this.categoryName + " (_id integer primary key autoincrement, _recipe_name text, _recipe text);";

        recipeNameTxt = findViewById(R.id.txt_recipe_name);
        recipeNameTxt.setText(this.recipeName);

        recipeText = findViewById(R.id.txt_recipe);
        recipeText.addTextChangedListener(new TextWatcher() {

            // the user's changes are saved here
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                acceptCangesBtn.setText(R.string.accept_recipe_changed);
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // this space intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // this one too
            }
        });

        acceptCangesBtn = findViewById(R.id.btn_accept_changes);
        acceptCangesBtn.setOnClickListener(this);

        backBtn = findViewById(R.id.btn_back_to_cat);
        backBtn.setOnClickListener(this);

        String recipes = helperMethodClass.onReadDataFromTable(this, dbName, this.categoryName, "_recipe", currentRow);
        recipeText.setText(recipes);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_accept_changes:{
                String recipeText = this.recipeText.getText().toString();
                helperMethodClass.onUpdateData(this, dbName, categoryName, recipeId, "_recipe", recipeText);
                acceptCangesBtn.setText("Saved");
            }break;
            case R.id.btn_back_to_cat:{
                Intent intent= new Intent(this ,CategoryActivity.class);
                intent.putExtra("category_name", categoryName);
                startActivity(intent);
            }break;
        }
    }
}
