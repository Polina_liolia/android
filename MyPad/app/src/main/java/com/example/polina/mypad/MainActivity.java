package com.example.polina.mypad;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = "myLogs";

    Button viewCategoriesBtn, selectCategoriesBtn,
            deleteCategoriesBtn, addCategoryBtn;
    TextView categorySelectedTxt, allCaregoriesTxt;
    EditText newCategoryNameTxt;

    DBHelper dbHelper;          //create, open and update DBs
    SQLiteDatabase database;    //SQLite DB management
    Cursor cursor;              //reads DB table
    ContentValues contentValues;//to add new rows into table
    HelperMethodClass helperMethodClass = new HelperMethodClass();
    String dbName = "cooking.db";
    String sqlQuery = "create table if not exists categories (_id integer primary key autoincrement, _category_name text);";
    int currentRow = 0; //hacking var to store current table row number




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewCategoriesBtn = findViewById(R.id.btn_sort_out_cat);
        viewCategoriesBtn.setOnClickListener(this);

        selectCategoriesBtn = findViewById(R.id.btn_select_cat);
        selectCategoriesBtn.setOnClickListener(this);

        deleteCategoriesBtn = findViewById(R.id.btn_del_cat);
        deleteCategoriesBtn.setOnClickListener(this);

        addCategoryBtn = findViewById(R.id.btn_add_cat);
        addCategoryBtn.setOnClickListener(this);

        categorySelectedTxt = findViewById(R.id.txt_cat_selected);
        newCategoryNameTxt = findViewById(R.id.txt_new_cat_name);
        allCaregoriesTxt = findViewById(R.id.txt_all_categories);

        //creating DB:
        helperMethodClass.onCreateDb(this, dbName, sqlQuery);

        String categories = helperMethodClass.onReadDataFromTable(this, dbName, "categories", "_category_name");
        allCaregoriesTxt.setText(categories);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sort_out_cat:{
                currentRow = helperMethodClass.onViewDataFromTable(this, dbName,"categories", "_category_name", currentRow, categorySelectedTxt);
                currentRow++;
                Log.d(TAG, "curent row: " + currentRow);

            }break;
            case R.id.btn_select_cat:{
                String currentCategoryName = helperMethodClass.onGetCurrentColumnData(this, dbName, "categories", "_category_name", --currentRow);
                if(currentCategoryName != ""){
                    Intent intent= new Intent(this ,CategoryActivity.class);
                    final String MSG_KEY = "category_name";
                    intent.putExtra(MSG_KEY, currentCategoryName);
                    startActivity(intent);
                }
                else{
                    currentRow = 0;
                }
            }break;
            case R.id.btn_del_cat:{
                String categoryName = helperMethodClass.onDeleteStringTable(this, dbName, "categories", --currentRow);
                categorySelectedTxt.setText(R.string.selection);
                currentRow--;

                //drop db table with category's recipes:
                if(!categoryName.equals("")){
                    helperMethodClass.onDropTable(this, dbName, categoryName);
                }

                //show all categories after deleting:
                String categories = helperMethodClass.onReadDataFromTable(this, dbName, "categories", "_category_name");
                allCaregoriesTxt.setText(categories);
            }break;
            case R.id.btn_add_cat:{
                //save new category in db:
                String newCategoryName = newCategoryNameTxt.getText().toString();
                newCategoryNameTxt.setText("");
                helperMethodClass.onCreateNoteInTable(this, dbName, "categories", "_category_name", newCategoryName);

                //show all categories:
                String categories = helperMethodClass.onReadDataFromTable(this, dbName, "categories", "_category_name");
                allCaregoriesTxt.setText(categories);
            }break;
        }
    }
}
