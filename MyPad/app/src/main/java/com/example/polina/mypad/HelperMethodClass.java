package com.example.polina.mypad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.TextView;

public class HelperMethodClass {
    private final static String TAG = "myLogs";
    DBHelper dbHelper;          //create, open and update DBs
    SQLiteDatabase database;    //SQLite DB management
    Cursor cursor;              //reads DB table
    ContentValues contentValues;//to add new rows into table

    void onCreateDb(Context context, String dbName, String sqlQuery){
        Log.d(TAG, "onCreateDb");
       dbHelper = new DBHelper(context, dbName, null, 1, sqlQuery);
       try{
           database = dbHelper.getWritableDatabase();
       }
       catch (SQLException exception){
           Log.d(TAG, exception.getMessage());
           database = dbHelper.getReadableDatabase();
       }
       dbHelper.close();
       database.close();
    }

    void onCreateTable(Context context, String dbName, String sqlQuery){
        Log.d(TAG, "onCreateDb");
        dbHelper = new DBHelper(context, dbName, null, 1, sqlQuery);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }
        database.execSQL(sqlQuery);
        dbHelper.close();
        database.close();
    }

    void onCreateNoteInTable(Context context, String dbName, String tableName, String columnName, String value){
        Log.d(TAG, "onCreateNoteInTable");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open db to write data into table:
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }
        //writing data to table:
        cursor = database.query(tableName, null, null, null, null, null, null);
        contentValues = new ContentValues();
        contentValues.put(columnName, value);
        long rowNumber = database.insert(tableName, null, contentValues);
        Log.d(TAG, "row number=" + rowNumber);

        //close everything:
        cursor.close();
        dbHelper.close();
        database.close();
    }

    public String onReadDataFromTable(Context context, String dbName, String tableName, String columnName) {
        Log.d(TAG, "onReadDataFromTable");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }

        //read data from table row
        StringBuilder data = new StringBuilder();
        cursor = database.query(tableName, null, null, null, null, null, null);
        if(cursor.moveToFirst()){
            do{
                int columnIndex = cursor.getColumnIndex(columnName);
                Log.d(TAG, "cursor.getColumnIndex(columnName)= " + columnIndex);
                data.append("\n");
                data.append(cursor.getString(columnIndex)); //data from column
            }while (cursor.moveToNext());
        }

        String dataStr = data.toString();
        Log.d(TAG, "all data from column: " + dataStr);

        //close everything:
        cursor.close();
        dbHelper.close();
        database.close();

        return  dataStr;
    }

    public String onReadDataFromTable(Context context, String dbName, String tableName, String columnName, int currentRow) {
        Log.d(TAG, "onReadDataFromTable");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }

        //read data from table row
        String data = "";
        cursor = database.query(tableName, null, null, null, null, null, null);
        if(cursor.moveToPosition(currentRow)){
                int columnIndex = cursor.getColumnIndex(columnName);
                Log.d(TAG, "cursor.getColumnIndex(columnName)= " + columnIndex);
                data = cursor.getString(columnIndex); //data from column
        }

        Log.d(TAG, "all data from column: " + data);

        //close everything:
        cursor.close();
        dbHelper.close();
        database.close();

        return  data;
    }

    public int onViewDataFromTable(Context context, String dbName, String tableName, String columnName, int currentRow, TextView textView) {
        Log.d(TAG, "onViewDataFromTable");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }

        //read data from table row
        String data = "";
        cursor = database.query(tableName, null, null, null, null, null, null);
        if(cursor.moveToPosition(currentRow)){
            data = cursor.getString(cursor.getColumnIndex(columnName));
        }
        else{
            data = "--That's all!--";
            currentRow = -1;
        }

       textView.setText(data);
       Log.d(TAG, "current data: " + data);

        //close everything:
        cursor.close();
        dbHelper.close();
        database.close();

        return  currentRow;

    }

    public String onDeleteStringTable(Context context, String dbName, String tableName, int currentRow) {
        Log.d(TAG, "onDeleteStringTable");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }

        //delete row from table:
        cursor = database.query(tableName, null, null, null, null, null, null);
        String categoryName = "";
        if(cursor.moveToPosition(currentRow)){
            categoryName = cursor.getString(1);
           database.delete(tableName, "_id = " + cursor.getString(0), null);
        }
        cursor.close();
        dbHelper.close();
        database.close();
        return categoryName;
    }

    public String onGetCurrentColumnData(Context context, String dbName, String tableName, String columnName, int currentRow){
        Log.d(TAG, "onGetCurrentColumnData");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }

        //read data from table row
        String data = "";
        cursor = database.query(tableName, null, null, null, null, null, null);
        if(cursor.moveToPosition(currentRow)){
            data = cursor.getString(cursor.getColumnIndex(columnName));
        }

        Log.d(TAG, "current data: " + data);

        //close everything:
        cursor.close();
        dbHelper.close();
        database.close();

        return data;
    }

    public void onUpdateData(Context context, String dbName, String tableName, String id, String columnName, String value) {
        Log.d(TAG, "onUpdateData");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id",id);
        contentValues.put(columnName,value);
        database.update(tableName, contentValues, "_id = ?",new String[] { id });
        cursor.close();
        dbHelper.close();
        database.close();
    }

    public void onDropTable(Context context, String dbName, String tableName){
        Log.d(TAG, "onDropTable");
        //Open DB to define version
        database = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        int version = database.getVersion();
        database.close();
        Log.d(TAG, "version=" + version);

        //open DB to read data from table
        dbHelper = new DBHelper(context, dbName, null, version, null);
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLException exception){
            Log.d(TAG, exception.getMessage());
            database = dbHelper.getReadableDatabase();
        }
        database.execSQL("DROP TABLE IF EXISTS " + tableName);
    }
}
