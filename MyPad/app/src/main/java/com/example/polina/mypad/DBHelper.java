package com.example.polina.mypad;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private final static String TAG = "myLogs";
    String sqlQuery;

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version, String sqlQuery ) {
        super(context, name, factory, version);
        this.sqlQuery = sqlQuery;
        Log.d(TAG, "constructor");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "onCreate");
        sqLiteDatabase.execSQL(sqlQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d(TAG, "onUpgrade");
        sqLiteDatabase.execSQL(sqlQuery);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade");
    }
}
