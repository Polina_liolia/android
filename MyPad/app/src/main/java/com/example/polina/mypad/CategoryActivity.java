package com.example.polina.mypad;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static java.lang.Integer.parseInt;

public class CategoryActivity extends AppCompatActivity  implements View.OnClickListener {

    private final static String TAG = "myLogs";
    private String categoryName;

    Button viewRecipesBtn, selectRecipeBtn,
            deleteRecipeBtn, addRecipeBtn, backBtn;
    TextView recipeSelectedTxt, allRecipesTxt, recipesTitleTxt;
    EditText newRecipeNameTxt;

    HelperMethodClass helperMethodClass = new HelperMethodClass();
    String dbName = "cooking.db";
    String sqlQuery;
    int currentRow = 0; //hacking var to store current table row number


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Intent intent = getIntent();
        this.categoryName = intent.getStringExtra("category_name");

        recipeSelectedTxt = findViewById(R.id.txt_recipes_title);
        recipeSelectedTxt.setText(this.categoryName + " recipes");

        sqlQuery = "create table if not exists " + this.categoryName + " (_id integer primary key autoincrement, _recipe_name text, _recipe text);";

        viewRecipesBtn = findViewById(R.id.btn_sort_out_recipes);
        viewRecipesBtn.setOnClickListener(this);

        selectRecipeBtn = findViewById(R.id.btn_select_recipe);
        selectRecipeBtn.setOnClickListener(this);

        deleteRecipeBtn = findViewById(R.id.btn_del_recipe);
        deleteRecipeBtn.setOnClickListener(this);

        addRecipeBtn = findViewById(R.id.btn_add_recipe);
        addRecipeBtn.setOnClickListener(this);

        backBtn = findViewById(R.id.btn_back_to_contents);
        backBtn.setOnClickListener(this);

        recipeSelectedTxt = findViewById(R.id.txt_recipe_selected);
        newRecipeNameTxt = findViewById(R.id.txt_new_recipe_name);
        allRecipesTxt = findViewById(R.id.txt_all_recipes);

        //creating DB:
        helperMethodClass.onCreateDb(this, dbName, sqlQuery);
        //creating a new table in db:
        helperMethodClass.onCreateTable(this, dbName, sqlQuery);

        String recipes = helperMethodClass.onReadDataFromTable(this, dbName, this.categoryName, "_recipe_name");
        allRecipesTxt.setText(recipes);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sort_out_recipes:{
                currentRow = helperMethodClass.onViewDataFromTable(this, dbName, this.categoryName, "_recipe_name", currentRow, recipeSelectedTxt);
                currentRow++;
                Log.d(TAG, "current row: " + currentRow);

            }break;
            case R.id.btn_select_recipe:{
                String currentRecipeId = helperMethodClass.onGetCurrentColumnData(this, dbName, this.categoryName, "_id", --currentRow);
                String currentRecipeName = helperMethodClass.onGetCurrentColumnData(this, dbName, this.categoryName, "_recipe_name", currentRow);
                if(currentRecipeId != "" && currentRecipeName != ""){
                    Intent intent= new Intent(this ,RecipeActivity.class);
                    intent.putExtra("category_name", this.categoryName);
                    intent.putExtra("recipe_id", currentRecipeId);
                    intent.putExtra("recipe_name", currentRecipeName);
                    intent.putExtra("current_row", currentRow);
                    startActivity(intent);
                }
                else{
                    currentRow = 0;
                }
            }break;
            case R.id.btn_del_recipe:{
                helperMethodClass.onDeleteStringTable(this, dbName, this.categoryName, --currentRow);
                recipeSelectedTxt.setText(R.string.selection_recipe);
                currentRow--;
                //show all categories after deleting:
                String recipes = helperMethodClass.onReadDataFromTable(this, dbName, this.categoryName, "_recipe_name");
                allRecipesTxt.setText(recipes);
            }break;
            case R.id.btn_add_recipe:{
                //save new recipe in db:
                String newRecipeName = newRecipeNameTxt.getText().toString();
                newRecipeNameTxt.setText("");
                helperMethodClass.onCreateNoteInTable(this, dbName, this.categoryName, "_recipe_name", newRecipeName);

                //show all categories:
                String recipes = helperMethodClass.onReadDataFromTable(this, dbName, this.categoryName, "_recipe_name");
                allRecipesTxt.setText(recipes);
            }break;
            case R.id.btn_back_to_contents:{
                Intent intent= new Intent(this, MainActivity.class);
                startActivity(intent);
            }break;
        }
    }
}
