package com.example.polina.tetris;

public class Analyser {
    private static final String TAG = "myLogs Analyser";
    public static final int totalShapes = 4;
    public static Shape define(int shapeIndex, int[][] playField, int row, int col, int shapeCount, boolean moveRight, boolean moveLeft) {
        Shape shape = null;
        switch (shapeIndex){
            case 0: shape = new Shape1(playField, row, col, shapeCount, moveRight, moveLeft); break;
            case 1: shape = new Shape2(playField, row, col, shapeCount, moveRight, moveLeft); break;
            case 2: shape = new Shape3(playField, row, col, shapeCount, moveRight, moveLeft); break;
            case 3: shape = new Shape4(playField, row, col, shapeCount, moveRight, moveLeft); break;
        }
        return shape;
    }


}
