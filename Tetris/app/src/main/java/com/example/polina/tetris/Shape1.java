package com.example.polina.tetris;

public class Shape1 extends Shape {

    public Shape1(int[][] playField, int row, int col, int shapeCount, boolean moveRight, boolean moveLeft) {

        super(playField, row, col, shapeCount, moveRight, moveLeft);
    }

    @Override
    public boolean buildShape(int row, int col, int shapeCount, boolean moveRight, boolean moveLeft) {
        width = 2;
        height = 3;
        if ((playField[col][row] != 0 && playField[col][row] != shapeCount) ||
                (playField[col][row + 1] != 0 && playField[col][row + 1] != shapeCount) ||
                (playField[col][row + 2] != 0 && playField[col][row + 2] != shapeCount) ||
                (playField[col + 1][row] != 0 && playField[col + 1][row] != shapeCount)){
            return false;
        }
        else {
//            for (int i = 0; i < playField.length; i++ ) {
//                for (int j = 0; j < playField[i].length; j++){
//                    if (playField[i][j] == shapeCount) {
//                        playField[i][j] = 0;
//                    }
//                }
//            }
            playField[col][row - 1] = playField[col][row - 1] == shapeCount ? 0 : playField[col][row - 1];
            playField[col][row + 1 - 1] = playField[col][row + 1 - 1] == shapeCount ? 0 : playField[col][row + 1 - 1];
            playField[col][row + 2 - 1] = playField[col][row + 2 - 1] == shapeCount ? 0 : playField[col][row + 2 - 1];
            playField[col+1][row - 1] = playField[col+1][row - 1] == shapeCount ? 0 : playField[col+1][row - 1];

            if(moveRight){
                col++;
                return this.buildShape(row, col, shapeCount, false, false);
            }
            else if(moveLeft){
                col--;
                return this.buildShape(row, col, shapeCount, false, false);
            }
            else{
                playField[col][row] = shapeCount;
                playField[col][row + 1] = shapeCount;
                playField[col][row + 2] = shapeCount;
                playField[col + 1][row] = shapeCount;
            }

        }

       return true;
    }
}
