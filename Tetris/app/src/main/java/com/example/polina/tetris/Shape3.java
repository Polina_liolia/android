package com.example.polina.tetris;

public class Shape3 extends Shape {
    public Shape3(int[][] playField, int row, int col, int shapeCount, boolean moveRight, boolean moveLeft) {

        super(playField, row, col, shapeCount, moveRight, moveLeft);
    }

    @Override
    public boolean buildShape(int row, int col, int shapeCount, boolean moveRight, boolean moveLeft) {
        this.width = 3;
        this.height = 2;
        if ((playField[col+1][row] != 0 && playField[col+1][row] != shapeCount) ||
                (playField[col+1][row + 1] != 0 && playField[col+1][row + 1] != shapeCount) ||
                (playField[col+2][row + 1] != 0 && playField[col+2][row + 1] != shapeCount) ||
                (playField[col][row + 1] != 0 && playField[col][row + 1] != shapeCount)){
            return false;
        }
        else{
//            for (int i = 0; i < playField.length; i++ ) {
//                for (int j = 0; j < playField[i].length; j++){
//                    if (playField[i][j] == shapeCount) {
//                        playField[i][j] = 0;
//                    }
//                }
//            }
            playField[col+1][row - 1] = playField[col+1][row - 1] == shapeCount ? 0 : playField[col+1][row - 1];
            playField[col+1][row + 1 - 1] = playField[col+1][row + 1 - 1] == shapeCount ? 0 : playField[col+1][row + 1 - 1];
            playField[col+2][row + 1 - 1] = playField[col+2][row + 1 - 1] == shapeCount ? 0 : playField[col+2][row + 1 - 1];
            playField[col][row + 1 - 1] = playField[col][row + 1 - 1] == shapeCount ? 0 : playField[col][row + 1 - 1];

            if(moveRight){
                col++;
                return this.buildShape(row, col, shapeCount, false, false);
            }
            else if(moveLeft){
                col--;
                return this.buildShape(row, col, shapeCount, false, false);
            }
            else {
                playField[col + 1][row] = shapeCount;
                playField[col + 1][row + 1] = shapeCount;
                playField[col + 2][row + 1] = shapeCount;
                playField[col][row + 1] = shapeCount;
            }
        }
        return true;
    }
}
