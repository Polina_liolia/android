package com.example.polina.tetris;

public abstract class Shape {
    protected int height;
    protected int width;
    protected int[][]playField;
    protected boolean isShapeBuilt;
    protected boolean moveRight;
    protected boolean moveLeft;

    public Shape(int[][] playField, int row, int col, int shapeCount, boolean moveRight, boolean moveLeft) {
        this.playField = playField;
        this.moveRight = moveRight;
        this.moveLeft = moveLeft;
        this.isShapeBuilt = this.buildShape(row, col, shapeCount, moveRight, moveLeft);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int[][] getPlayField() {
        return playField;
    }

    public void setPlayField(int[][] playField) {
        this.playField = playField;
    }

    public abstract boolean buildShape(int row, int col, int shapeCount, boolean moveRight, boolean moveLeft);

    public boolean isShapeBuilt() {
        return isShapeBuilt;
    }
}
