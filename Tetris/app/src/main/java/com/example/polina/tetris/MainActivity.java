package com.example.polina.tetris;

import android.Manifest;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "myLogs";

    TableLayout tableLayout;
    TableRow[] tableRow;
    Button [] buttons;
    final int rowsNumber = 15, //m
        colsNumber = 8, //n
        btnsNumber = rowsNumber * colsNumber, //k
        totalShapes = 6;
    int currentCol = 4;
    int[][] playField = new int[colsNumber + 2][rowsNumber + 2]; //a2
    Handler handler;
    Handler messageHandler;
    Thread thread;
    Shape currentShape;
    boolean isGameOver;
    public static boolean moveLeft;
    public static boolean moveRight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "MainActivity onCreate" );
        isGameOver = false;

        tableLayout = new TableLayout(this);
        tableRow = new TableRow[rowsNumber];
        buttons = new Button[btnsNumber];

        //init buttons
        for(int i = 0; i < btnsNumber; i++){
            buttons[i] = new Button(this);
            buttons[i].setOnClickListener(this);
            buttons[i].setId(i);
            buttons[i].setBackgroundResource(R.drawable.bg);
            buttons[i].setLayoutParams(new TableRow.LayoutParams(-1, -1, 1.0f));
        }

        //init table row, add btns into it
        int btnsCount = 0;
        for(int i = 0; i < rowsNumber; i++){
            tableRow[i] = new TableRow(this);
            tableRow[i].setLayoutParams(new TableLayout.LayoutParams(-1, -1, 1.0f));
            for(int j = 0; j < colsNumber; j++){
                tableRow[i].addView(buttons[btnsCount++]);
            }
        }

        setContentView(tableLayout);

        //add rows to table view
        for(int i = 0; i < rowsNumber; i++){
            tableLayout.addView(tableRow[i]);
        }

        new TetrisAsyncTask().execute();

//        //init msgs object
//        handler = new Handler () {
//            @Override
//            public void handleMessage(Message msg) {
//                backgr(msg.what);
//            }
//        };
//        messageHandler = new Handler(Looper.getMainLooper()){
//            @Override
//            public void handleMessage(Message msg) {
//                Context context = getApplicationContext();
//                CharSequence text = "Game over!";
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        };
//
//        //launch thread
//        threads(0);
    }

    //rerender screen
//    public void backgr(int number){
//        int k = 0;
//        for (int i = 1; i < rowsNumber + 1; i++){       //rows
//            for(int j = 1; j < colsNumber + 1; j++){    //cols
//                k = (i - 1) * 8 + (j - 1);
//                if(k < buttons.length - 1 ) {
//                    if (playField[j][i] == 0) {
//                        buttons[k].setBackgroundResource(R.drawable.bg);
//                    } else {
//                        buttons[k].setBackgroundResource(R.drawable.fg);
//                    }
//                }
//            }
//        }
//    }

    //launch thread
//    public void threads(final int shapeCount){
//            thread = new Thread((Runnable) ()->{
//                int maxI = rowsNumber;
//                Random random = new Random();
//                int shapeIndex = (random.nextInt((Analyser.totalShapes - 1 - 0) + 1) + 0);
//                this.currentCol = 4;//default
//                for(int i = 1; i <= maxI; i++){
//                    //method where shape is selected and positioned
//                    currentShape = Analyser.define(shapeIndex, playField, i, currentCol, shapeCount, this.moveRight, this.moveLeft);
//                    this.moveRight = this.moveLeft = false;
//                    if (!currentShape.isShapeBuilt){
//                        if(i == 1){
//                            isGameOver = true;
//                        }
//                        break; //no place to continue moving
//                    }
//                    maxI = rowsNumber - currentShape.height + 1;
//                    playField = currentShape.getPlayField();
//                    try{
//                        TimeUnit.SECONDS.sleep(1);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    handler.sendEmptyMessage(i);
//                }
//                if(isGameOver){
//                    Message message = messageHandler.obtainMessage(1);
//                    message.sendToTarget();
//                }
//                if(shapeCount <= this.totalShapes && !isGameOver){
//                    threads(shapeCount + 1);
//                }
//            });
//            thread.start();
//
//    }

    //clicks reaction - if user taps to left side of a shape - it moves left, and so on
    @Override
    public void onClick(View view) {
        int id = view.getId();
        int colIndex = id % this.colsNumber;
        if (colIndex < 4 && this.currentCol > 0){
            moveLeft = true;
        }
        else if (this.currentCol + this.currentShape.width < this.colsNumber ){
            moveRight = true;
        }
    }


    private class TetrisAsyncTask extends AsyncTask<Void, Integer, Void> {


        int shapeCount = 1;

        @Override
        protected Void doInBackground(Void... voids) {
            longTask();
            return null;
        }

        protected void longTask(){
            while(shapeCount <= totalShapes && !isGameOver) {
                int maxI = rowsNumber;
                Random random = new Random();
                int shapeIndex = (random.nextInt((Analyser.totalShapes - 1 - 0) + 1) + 0);
                currentCol = 4;//default
                for (int i = 1; i <= maxI; i++) {
                    //method where shape is selected and positioned
                    currentShape = Analyser.define(shapeIndex, playField, i, currentCol, shapeCount, MainActivity.moveRight, MainActivity.moveLeft);
                    MainActivity.moveRight = MainActivity.moveLeft = false;
                    if (!currentShape.isShapeBuilt) {
                        if (i == 1) {
                            isGameOver = true;
                        }
                        break; //no place to continue moving
                    }
                    maxI = rowsNumber - currentShape.height + 1;
                    playField = currentShape.getPlayField();
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    publishProgress();
                }
                shapeCount++;
                if (isGameOver) {
                    return;
                }
            }

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast toast = Toast.makeText(MainActivity.this, "GameOver", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
            toast.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int k = 0;
            for (int i = 1; i < rowsNumber + 1; i++){       //rows
                for(int j = 1; j < colsNumber + 1; j++){    //cols
                    k = (i - 1) * 8 + (j - 1);
                    if(k < buttons.length - 1 ) {
                        if (playField[j][i] == 0) {
                            buttons[k].setBackgroundResource(R.drawable.bg);
                        } else {
                            buttons[k].setBackgroundResource(R.drawable.fg);
                        }
                    }
                }
            }
        }
    }

}
