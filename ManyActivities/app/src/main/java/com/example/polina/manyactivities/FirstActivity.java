package com.example.polina.manyactivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FirstActivity  extends Activity implements View.OnClickListener {

    private static final String TAG = "myLogs";
    TextView textView;
    EditText editText;
    Button button;

    int k = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "FirstActivity_onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        textView = (TextView) findViewById(R.id.textView);
        editText = (EditText)findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        Log.e(TAG, "FirstActivity_onStart");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.e(TAG, "FirstActivity_onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "FirstActivity_onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "FirstActivity_onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "FirstActivity_onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "FirstActivity_onDestroy");
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {
        k++;
        this.textView.setText(editText.getText() + " " + k);
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }
}
