package com.example.polina.manyactivities;

import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "myLogs";

    TextView textView;
    EditText editText;
    Button button;
    Button button_back;

    int k = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "MainActivity_onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        editText = (EditText)findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
        button_back = (Button) findViewById(R.id.button_back);
        button_back.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        Log.e(TAG, "MainActivity_onStart");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.e(TAG, "MainActivity_onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "MainActivity_onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "MainActivity_onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "MainActivity_onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "MainActivity_onDestroy");
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        k++;
        Intent intent = null;
        this.textView.setText(editText.getText() + " " + k);
        switch (view.getId()){
            case R.id.button:
                intent = new Intent(this, MainActivity.class); break;
                case: R.id.button_back:
                    
        }
        Intent intent = new Intent(this, FirstActivity.class);
        startActivity(intent);
    }
}
