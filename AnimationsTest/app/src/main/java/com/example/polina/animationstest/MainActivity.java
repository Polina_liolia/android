package com.example.polina.animationstest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    Button btn;
    EditText editText;
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get animation object
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.alphap);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.alphap2);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.alphap3);
        Animation translateAnim = AnimationUtils.loadAnimation(this, R.anim.translatec);
        Animation scaleAnim = AnimationUtils.loadAnimation(this, R.anim.myscalesize);
        Animation rotationAnim = AnimationUtils.loadAnimation(this, R.anim.myrotationanim);

        //set animation for TextView
        textView = (TextView) findViewById(R.id.textView1);
        textView.startAnimation(animation);
        textView.startAnimation(scaleAnim);

        //set animation for EditText
        editText = (EditText) findViewById(R.id.txt_edit);
        editText.startAnimation(animation1);

        //set animation for Button
        btn = (Button) findViewById(R.id.button);
        btn.startAnimation(animation2);
        btn.startAnimation(translateAnim);
        btn.startAnimation(rotationAnim);

        //Add click listener to button
        View.OnClickListener btnClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn.setText("Ouch! You pushed me!");
            }
        };
        btn.setOnClickListener(btnClick);

        //background animation:
        layout = (LinearLayout) findViewById(R.id.layout);
        layout.startAnimation(animation);
    }
}
