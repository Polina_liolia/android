package com.example.polina.canvasinandroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "myLog";
    private float touchX = 0.0f, touchY = 0.0f,
    frequency = 100.0f,
    X1 = 0.0f, Y1 = 0.0f,
    X2 = 0.0f, Y2 = 0.0f;
    GraphicsView myGraphicsView;
    GraphicsView.RedrawAsynkTask redrawAsynkTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myGraphicsView = new GraphicsView(this);
        setContentView(myGraphicsView);
    }

    private class GraphicsView extends View {
        BitmapFactory.Options options;
        Display display;

        public GraphicsView(Context context) {
            super(context);
            //image resource loading optimization
            display = getWindowManager().getDefaultDisplay();
            options = new BitmapFactory.Options();

        }

        @Override
        protected void onDraw(Canvas canvas) {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), R.drawable.android_icon, options);
            options.inSampleSize = options.outHeight/display.getHeight() > 0 ?
                    options.outHeight/display.getHeight() : 1;
            options.inJustDecodeBounds = false;
            Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.android_icon, options);
            if(touchX - myBitmap.getScaledWidth(canvas) / 2 < 0){
                touchX = myBitmap.getScaledWidth(canvas)/2;
            }

            if(touchY - myBitmap.getScaledHeight(canvas) / 2 < 0){
                touchY = myBitmap.getScaledHeight(canvas) / 2;
            }

           canvas.drawBitmap(myBitmap,
                    touchX - myBitmap.getScaledWidth(canvas) / 2,
                    touchY - myBitmap.getScaledHeight(canvas) / 2,
                    null);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                X1 = touchX;
                touchX = event.getX();
                X2 = touchX;
                Y1 = touchY;
                touchY = event.getY();
                Y2 = touchY;
               //invalidate(); update UI
                redrawAsynkTask = new RedrawAsynkTask();
                redrawAsynkTask.execute();//update UI async
            }
            return true;
        }

        public class RedrawAsynkTask extends AsyncTask<Void, Integer, Void>{
            int progressStatus = 0;
            @Override
            protected Void doInBackground(Void... voids) {
                while(progressStatus++ < 100){
                    Log.d(TAG, "doInBackground");
                    publishProgress(progressStatus);
                    SystemClock.sleep(300);
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                Log.d(TAG, "onProgressUpdate");
                touchX = X1 + ((X2 - X1) / frequency) * (float)progressStatus;
                touchY = Y1 + ((Y2 - Y1) / frequency) * (float)progressStatus;
                myGraphicsView.invalidate();
            }
        }


    }
}
