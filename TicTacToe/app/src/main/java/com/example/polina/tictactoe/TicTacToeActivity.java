package com.example.polina.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TicTacToeActivity extends AppCompatActivity
{
    TextView txtGameInfo;
    TextView txtResult;
    Button [][] buttons = new Button[3][3];
    int counter=0;
    boolean playWithAI = false;
    Analyzer analyzer = new Analyzer();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        this.InitializeComponent();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_human:
                this.playWithAI = false;
                return true;
            case R.id.menu_ai:
                this.playWithAI = true;
                return true;
            case R.id.menu_play_again:
                this.PlayAgain();
                return true;
            case R.id.menu_exit:
                this.Exit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void InitializeComponent(){
        txtGameInfo = (TextView)findViewById(R.id.textView);
        txtResult = (TextView) findViewById(R.id.textResult);

        buttons[0][0] = (Button)findViewById(R.id.btn00);
        buttons[0][1] = (Button)findViewById(R.id.btn01);
        buttons[0][2] = (Button)findViewById(R.id.btn02);
        buttons[1][0] = (Button)findViewById(R.id.btn11);
        buttons[1][1] = (Button)findViewById(R.id.btn12);
        buttons[1][2] = (Button)findViewById(R.id.btn13);
        buttons[2][0] = (Button)findViewById(R.id.btn20);
        buttons[2][1] = (Button)findViewById(R.id.btn21);
        buttons[2][2] = (Button)findViewById(R.id.btn22);


        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                buttons[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BtnOnClick(view);
                    }
                });
            }
        }
    }

    private void BtnOnClick(View view) {
        Button btn = (Button)view;
        btn.setEnabled(false);
        Symbols symbol = counter%2 == 0 ? Symbols.X : Symbols.O;
        btn.setText(symbol.name());
        counter++;
        this.RegisterMove(btn, symbol);
        this.ProcessGameState();

        if(!this.analyzer.IsGameOver() && this.playWithAI)// if AI mode selected, AI moves
             this.MakeMoveAI();
    }

    //defines current game state (is game over and does somebody win)
    //displays result on UI
    private void ProcessGameState() {
        boolean isGameOver = this.analyzer.IsGameOver();
        Symbols winnerSymbol = this.analyzer.DefineWinner();
        if (winnerSymbol != Symbols.none){
            this.SetResultText(winnerSymbol == Symbols.X ? "Победили крестики" : "Победили нолики");
            this.SetButtonsEnabledState(false);
        }
        else if (isGameOver){
            this.SetResultText("Игра окончена! Ничья.");
            this.SetButtonsEnabledState(false);
        }
        else{
            this.SetResultText("Пока победителя нет.");
        }
    }

    private void MakeMoveAI(){
        Symbols symbol = counter%2 == 0 ? Symbols.X : Symbols.O;
        int[] btnIndexes = ArtificalIntelligence.MakeMove(this.analyzer.getGameField(), symbol);
        if(btnIndexes[0] >= 0 && btnIndexes[0] < 3 && btnIndexes[1] >= 0 && btnIndexes[1] < 3){ //if valid move was defined
            Button btn = buttons[btnIndexes[0]][btnIndexes[1]];
            btn.setEnabled(false);
            btn.setText(symbol.name());
            this.RegisterMove(btn, symbol);
            this.counter++;
            this.ProcessGameState();
        }

    }

    private void RegisterMove(Button btn, Symbols symbol) {
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if (buttons[i][j] == btn) //button found
                {
                    analyzer.SetSymbol(i, j, symbol);
                    return;
                }
            }
        }
    }

    private void SetResultText(String text){
        this.txtResult.setText(text);
    }

    private void SetButtonsEnabledState(boolean state){
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                buttons[i][j].setEnabled(state);
            }
        }
    }

    private void PlayAgain() {
        Intent intent = this.getIntent();
        this.finish();
        this.startActivity(intent);
    }

    private void Exit() {
        this.finish();
    }



}
