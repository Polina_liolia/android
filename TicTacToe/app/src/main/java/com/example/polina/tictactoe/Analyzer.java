package com.example.polina.tictactoe;

enum Symbols { O, X, none}

public class Analyzer {

    private Symbols[][] gameField = new Symbols[3][3];
    private int counter = 0; //counts players' moves

    public Symbols[][] getGameField() {
        return gameField;
    }

    public Analyzer() {
        this.ClearGameField();
    }

    //sets Symbols.none value for all game fields
    public void ClearGameField(){
        for(int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                this.gameField[i][j] = Symbols.none;
            }
        }
        this.counter = 0; //new game starts
    }

    //sets symbol from params for i-row j-column of the game field
    //returns true if symbol can be set and false if not
    public boolean SetSymbol(int i, int j, Symbols symbol){
        boolean result = false;
        if (i >= 0 && i < 3 && j >=0 && j < 3){
            this.gameField[i][j] = symbol;
            this.counter++;
            result = true;
        }
        return result;
    }

    //defines if any player wins
    //returns symbol of winner or none if no winner exists yet
    public Symbols DefineWinner(){
        Symbols winner = Symbols.none;
        if (gameField[0][0] == gameField[0][1] && gameField[0][0] == gameField[0][2]){
            winner = gameField[0][0];
        }
        else if (gameField[1][0] == gameField[1][1] && gameField[1][0] == gameField[1][2]){
            winner = gameField[1][0];
        }
        else if (gameField[2][0] == gameField[2][1] && gameField[2][0] == gameField[2][2]){
            winner = gameField[2][0];
        }
        else if (gameField[0][0] == gameField[1][0] && gameField[0][0] == gameField[2][0]){
            winner = gameField[0][0];
        }
        else if (gameField[0][1] == gameField[1][1] && gameField[0][1] == gameField[2][1]){
            winner = gameField[0][1];
        }
        else if (gameField[0][2] == gameField[1][2] && gameField[0][2] == gameField[2][2]){
            winner = gameField[0][2];
        }
        else if (gameField[0][0] == gameField[1][1] && gameField[0][0] == gameField[2][2]){
            winner = gameField[0][0];
        }
        else if (gameField[0][2] == gameField[1][1] && gameField[0][2] == gameField[2][0]){
            winner = gameField[0][2];
        }
        return winner;
    }

    //defines if game is over (no moves left)
    public boolean IsGameOver(){
        return this.counter >= 9;
    }
}
