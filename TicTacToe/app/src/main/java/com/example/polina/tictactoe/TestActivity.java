package com.example.polina.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TestActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView;
    private Button btn1;
    private Button btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        textView = (TextView) findViewById(R.id.textView);
        btn1 = (Button) findViewById(R.id.btn1);

        btn2 = (Button) findViewById(R.id.btn2);


        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn1:
            {
                textView.setText("Button1 pressed");
                btn1.setEnabled(false);
                btn1.setText("Button pushed");
            }break;
            case R.id.btn2:
            {
                textView.setText("Button2 pressed");
                btn2.setEnabled(false);
                btn2.setText("Button pushed");
            }break;
            default:
                textView.setText("No button pressed");
        }
    }
}
