package com.example.polina.tictactoe;

class ArtificalIntelligence {
    public static int[] MakeMove(Symbols[][] gameField, Symbols aiSymbol) {
        Symbols humanSymbol = aiSymbol == Symbols.X ? Symbols.O : Symbols.X;
        int [] result = new int[] {-1, -1};

        //searching for win ability:
        result = getMove(gameField, aiSymbol);

        //if no win ability, searching for defending necessity
        if (result[0] == -1 || result[1] == -1){
            result = getMove(gameField, humanSymbol);
        }
        //if no need in defending detected, make any possible move
        if (result[0] == -1 || result[1] == -1){
            result = getAnyMove(gameField);
        }
        return result;
    }

    //returns win move for player with symbol from args
    private static int[] getMove(Symbols[][] gameField, Symbols symbol){
        int [] result = new int[] {-1, -1};
        for(int i = 0; i < 3; i++){
            if (checkRow(gameField, i, symbol)){
                result = getFreeInRow(gameField, i);
            }
            else if (checkColumn(gameField, i, symbol)){
                result = getFreeInColumn(gameField, i);
            }
        }
        if (result[0] == -1 && result[1] == -1) //win move was not found yet
        {
            if (checkRightDiagonal(gameField, symbol)){
                result = getFreeInRightDiagonal(gameField);
            }
            else if (checkLeftDiagonal(gameField, symbol)){
                result = getFreeInLeftDiagonal(gameField);
            }
        }
        return result;
    }

    //returns indexes of any (not obviously win) free field
    private static int[] getAnyMove(Symbols[][] gameField){
        int [] result = new int[] {-1, -1};
        for (int i = 0; i < 3; i++){
            result = getFreeInRow(gameField, i);
            if (result[0] == -1 && result[1] == -1)
                result = getFreeInColumn(gameField, i);
        }
        if (result[0] == -1 && result[1] == -1)
            result = getFreeInRightDiagonal(gameField);
        if (result[0] == -1 && result[1] == -1)
            result = getFreeInLeftDiagonal(gameField);
        return result;
    }

    //checks if there are two same symbols in a row
    private static boolean checkRow(Symbols[][] gameField, int i, Symbols targetSymbol){
        boolean result = false;
        if (gameField[i][0] == gameField[i][1] && gameField[i][0] == targetSymbol
                || gameField[i][0] == gameField[i][2] && gameField[i][0] == targetSymbol
                || gameField[i][1] == gameField[i][2] && gameField[i][1] == targetSymbol){
            result = true;
        }
        return result;
    }

    //returns indexes of free field in a row (if not exist - [-1, -1])
    private static int[] getFreeInRow(Symbols[][] gameField, int index){
        for (int i = index; i <= index; i++ ){
            for (int j = 0; j < 3; j++){
                if(gameField[i][j] == Symbols.none)
                    return new int[] {i, j};
            }
        }
        return new int[] {-1, -1};
    }

    //checks if there are two same symbols in a column
    private static boolean checkColumn(Symbols[][] gameField, int j, Symbols targetSymbol){
        boolean result = false;
        if (gameField[0][j] == gameField[1][j] && gameField[0][j] == targetSymbol
                || gameField[0][j] == gameField[2][j] && gameField[0][j] == targetSymbol
                || gameField[1][j] == gameField[2][j] && gameField[1][j] == targetSymbol){
            result = true;
        }
        return result;
    }

    //returns indexes of free field in column (if not exist - [-1, -1])
    private static int[] getFreeInColumn(Symbols[][] gameField, int j_index){
        for (int i = 0; i < 3; i++ ){
            for (int j = j_index; j <= j_index; j++){
                if(gameField[i][j] == Symbols.none)
                    return new int[] {i, j};
            }
        }
        return new int[] {-1, -1};
    }

    //checks if there are two same symbols in a right diagonal
    private static boolean checkRightDiagonal(Symbols[][] gameField, Symbols targetSymbol){
        boolean result = false;
        if (gameField[0][0] == gameField[1][1] && gameField[0][0] == targetSymbol
                || gameField[0][0] == gameField[2][2] && gameField[0][0] == targetSymbol
                || gameField[1][1] == gameField[2][2] && gameField[1][1] == targetSymbol){
            result = true;
        }
        return result;
    }

    //returns indexes of free field in right diagonal (if not exist - [-1, -1])
    private static int[] getFreeInRightDiagonal(Symbols[][] gameField){
        for (int i = 0; i < 3; i++ ){
            for (int j = 0; j < 3; j++){
                if(i == j && gameField[i][j] == Symbols.none)
                    return new int[] {i, j};
            }
        }
        return new int[] {-1, -1};
    }


    //checks if there are two same symbols in left diagonal
    private static boolean checkLeftDiagonal(Symbols[][] gameField, Symbols targetSymbol){
        boolean result = false;
        if (gameField[0][2] == gameField[1][1] && gameField[0][2] == targetSymbol
                || gameField[0][2] == gameField[2][0] && gameField[0][2] == targetSymbol
                || gameField[1][1] == gameField[2][0] && gameField[1][1] == targetSymbol){
            result = true;
        }
        return result;
    }

    //returns indexes of free field in left diagonal (if not exist - [-1, -1])
    private static int[] getFreeInLeftDiagonal(Symbols[][] gameField){
        for (int i = 0; i < 3; i++ ){
            for (int j = 0; j < 3; j++){
                if(i+j == 2 && gameField[i][j] == Symbols.none)
                    return new int[] {i, j};
            }
        }
        return new int[] {-1, -1};
    }



}
