package com.example.polina.myfirstapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button btn;
    TextView textView;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.plainText);
        btn = (Button)findViewById(R.id.button);
        editText = (EditText) findViewById(R.id.editText);

        //creating event listener
        View.OnClickListener btnClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View arg){
                String text = editText.getText().toString();
                textView.setText("Input: " + text);
                editText.setText("");
            }
        };

        //adding event listener to button
        btn.setOnClickListener(btnClickListener);



    }
}
