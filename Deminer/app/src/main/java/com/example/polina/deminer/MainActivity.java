package com.example.polina.deminer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button[][] buttons = new Button[6][6];
    private int[][] playField = new int[6][6];
    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeComponent();
    }

    private void InitializeComponent(){

        txtResult = (TextView) findViewById(R.id.result);

        //init buttons array
        buttons[1][1] = (Button)findViewById(R.id.btn11);
        buttons[1][2] = (Button)findViewById(R.id.btn12);
        buttons[1][3] = (Button)findViewById(R.id.btn13);
        buttons[1][4] = (Button)findViewById(R.id.btn14);

        buttons[2][1] = (Button)findViewById(R.id.btn21);
        buttons[2][2] = (Button)findViewById(R.id.btn22);
        buttons[2][3] = (Button)findViewById(R.id.btn23);
        buttons[2][4] = (Button)findViewById(R.id.btn24);

        buttons[3][1] = (Button)findViewById(R.id.btn31);
        buttons[3][2] = (Button)findViewById(R.id.btn32);
        buttons[3][3] = (Button)findViewById(R.id.btn33);
        buttons[3][4] = (Button)findViewById(R.id.btn34);

        buttons[4][1] = (Button)findViewById(R.id.btn41);
        buttons[4][2] = (Button)findViewById(R.id.btn42);
        buttons[4][3] = (Button)findViewById(R.id.btn43);
        buttons[4][4] = (Button)findViewById(R.id.btn44);

        //set onClick listener for buttons
        for(int i = 1; i < 5; i++){
            for(int j = 1; j < 5; j++){
                buttons[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BtnOnClick(view);
                    }
                });
            }
        }

        //empty frame
        for (int i = 0; i < 6; i++ ){
            for (int j = 0; j < 6; j++ ){
                if (i == 0 || j == 0 || i == 5 || j == 5) {
                    buttons[i][j] = null;
                    playField[i][j] = 0;
                }
            }
        }

        //set 5 mines
        for (int counter = 0; counter < 5; counter++){
            Random r = new Random();
            int i, j;
            do{
               i = r.nextInt(4 - 1) + 1;
               j = r.nextInt(4 - 1) + 1;
            }while (playField[i][j] > 0);
            playField[i][j] = -1; //mine!
        }
    }

    private void BtnOnClick(View view) {
        Button btn = (Button)view;
        btn.setEnabled(false);

        int[] coords = this.getBtnCoords(btn);
        int number = this.calcNumber(coords);

        btn.setText(number);


    }

    private int[] getBtnCoords(Button btn) {
        int [] coords = new int[2];
        for (int i = 0; i < 6; i++ ){
            for (int j = 0; j < 6; j++ ){
                    if (buttons[i][j] == btn) {
                        coords[0] = i;
                        coords[1] = j;
                    }

            }
        }
        return  coords;
    }

    private int calcNumber(int[] coords) {
        return
    }
}