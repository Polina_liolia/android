package com.example.polina.threads;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int t1 = 0, t2 = 0;
    private TextView txt1, txt2, txt3;
    private Button btn1, btn2, btn3;
    private Thread thread = new Thread();
    private Handler handler3;
    private Thread thread3;
    private boolean shouldContinue;
    private Handler handler2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt1 = findViewById(R.id.txt1);
        btn1 = findViewById(R.id.btn_timer1);
        btn1.setOnClickListener(this);

        //start thread for one second (new thread for each iteration)
        txt2 = findViewById(R.id.txt2);
        btn2 = findViewById(R.id.btn_timer2);
        btn2.setOnClickListener(this);
        handler2 = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                txt2.setText("Timer " + msg.what);
                if(msg.what == 10){
                    btn2.setEnabled(true);
                    t2 = 0;
                }
                else{
                    thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                TimeUnit.SECONDS.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            handler2.sendEmptyMessage(t2);
                        }
                    });
                    thread.setDaemon(true);
                    thread.start();
                    t2++;
                }

            }
        };

        //set timer in separate thread
        txt3 = findViewById(R.id.txt3);
        btn3 = findViewById(R.id.btn_timer3);
        btn3.setOnClickListener(this);
        handler3 = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                txt3.setText("Timer: " + msg.what);
                if(msg.what == 10)
                    btn3.setEnabled(true);
            }
        };

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_timer1: {
                for (int i = 0; i < 5; i++) {
                    t1++;
                    try {
                        thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    txt1.setText("" + t1);
                }
            }
            break;
            case R.id.btn_timer2: {
                btn2.setEnabled(false);
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler2.sendEmptyMessage(t2);
                    }
                });
                thread.setDaemon(true);
                thread.start();
                t2++;
            }
            break;
            case R.id.btn_timer3: {
                btn3.setEnabled(false);
                shouldContinue = true;
                thread3 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 10; i++) {
                            if(!shouldContinue)
                                i = 9; //to end loop
                            //long process
                            try {
                                TimeUnit.SECONDS.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            handler3.sendEmptyMessage(i+1);
                        }
                    }
                });
                thread3.start();
            }
            break;
            case R.id.btn_kill: {
                shouldContinue = false;
            }break;
        }
    }
}
